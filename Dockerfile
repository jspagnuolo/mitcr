FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
	python3.8 \
	git \
	nano \
    libpthread-stubs0-dev \
    zlib1g-dev \
    autotools-dev \
    asciidoctor \
    default-jre \
    wget \
    libidn11 \
    unzip

# Get precompiled jar, unzip and test
RUN wget http://files.milaboratory.com/mitcr/1.0.3.3-beta/mitcr.jar
RUN java -jar mitcr.jar --version

